#!/usr/bin/env python3

from contextlib import contextmanager

import unicornhathd


@contextmanager
def led_hat(brightness=0.6, rotation=0, background=(0, 0, 0)):
    unicornhathd.brightness(brightness)
    unicornhathd.rotation(rotation)

    width, height = unicornhathd.get_shape()
    pixels = [(x, y) for x in range(width) for y in range(height)]

    def display_frame(img):
        scale = img.width//width
        for x, y in pixels:
            r, g, b, a = img.getpixel((y*scale, x*scale))
            r = int(r*a/255 + background[0]*(255-a)/255)
            g = int(g*a/255 + background[1]*(255-a)/255)
            b = int(b*a/255 + background[2]*(255-a)/255)
            unicornhathd.set_pixel(x, y, r, g, b)

        unicornhathd.show()

    yield display_frame

    unicornhathd.off()


if __name__ == "__main__":
    import argparse, time
    from statistics import median

    from PIL import Image, GifImagePlugin

    rotations = {"N": 180, "E": 270, "S": 0, "W": 90}
    colours = {"black": (0, 0, 0), "white" : (255, 255, 255), "grey": (127, 127, 127)}

    parser = argparse.ArgumentParser(prog='Gif Player', description='Play a PNG on a unicorn hat')
    parser.add_argument('filename', nargs="?", help="an animated .gif to play, defaults to man.gif", default="man.gif")
    parser.add_argument('speed', nargs="?", type=float, help="the number of seconds between frames of the gif, float 0.0 to 10, defaults to 0 to use the gifs speed or 12 frames per second", default=0)
    parser.add_argument('-b', '--brightness',type=float,  help="Brightness of the display, float 0.1 to 1.0, default 0.6", default="0.6")
    parser.add_argument('-r', '--rotation', help="Select a side of the hat to be the top (N, E, S, W) default N", default="N", choices=["N", "E", "S", "W"])
    parser.add_argument('-B', '--background', help="Set the background colour for transparancy default black", default="black", choices=list(colours.keys()))

    args = parser.parse_args()
    speed = median([0, args.speed, 10])
    brightness = median([0.1, args.brightness, 1.0])
    rotation = rotations.get(args.rotation, 180)
    background = colours.get(args.background, (0, 0, 0))

    GifImagePlugin.LOADING_STRATEGY = GifImagePlugin.LoadingStrategy.RGB_ALWAYS
    img = Image.open(args.filename)
    frames = img.n_frames

    with led_hat(brightness, rotation, background) as display:
        try:
            while True:
                display(img)
                time.sleep(speed if speed > 0 else img.duration/1000 if hasattr(img, "duration") else 0.08)
                img.seek((img.tell() + 1)%frames)  # progress frame
        except KeyboardInterrupt:
            pass
